var db = require("../config/db.js");

exports.all = function() {
    return db.rows("GetAllUsers", []);
}

exports.read = function(id) {
	return db.row('GetSingleUser', [id]);
}

exports.write = function(firstName, lastName, email, password, zip) {
    return db.row("CreateUser", [firstName, lastName, email, password, zip]);
}

exports.updateEmail = function(id, email) {
    return db.empty("UpdateEmail", [id, email]);
}

exports.updatePassword = function(id, password) {
    return db.empty("UpdatePassword", [id, password])
}
exports.updateImage = function(id, profileImg) {
    return db.empty("UpdatePassword", [id, profileImg])
}

exports.delete = function(id) {
    return db.empty("DeleteUser", [id]);
}